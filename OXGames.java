import java.util.Scanner;

public class OXGames {  
   public static final int EMPTY = 0;
   public static final int oo = 1;
   public static final int xx = 2;
  
   public static int Play = 0;
   public static int DRAW = 1;
   public static int CROSS_WON = 2;
   public static int NOUGHT_WON = 3;
 
   public static int rows = 3, column = 3; 
   public static int[][] board = new int[rows][column];     
   public static int currentState;                                     
   public static int currentPlayer; 
   public static int currentRow, currentCol; 
   public static Scanner in = new Scanner(System.in); 
   public static void main(String[] args) {
      showWelcome();
      board();
      showBoard(); 
      do {
         input(currentPlayer); 
         updateGame(currentPlayer, currentRow, currentCol); 
         showBoard();        
         showWin();
      } while (currentState == Play); 
   }
   public static void showWelcome(){
       System.out.println("Welcome to OX Game");
   }
   public static void board() {
      for (int row = 0; row < row; ++row) {
         for (int col = 0; col < row; ++col) {
            board[row][col] = EMPTY;  
         }
      }
      currentState = Play; 
      currentPlayer = oo;  
   }
   public static void showWin(){
       if (currentState == CROSS_WON) {
            System.out.println("O winnn");
         } else if (currentState == NOUGHT_WON) {
            System.out.println("X winnn");
         } else if (currentState == DRAW) {
            System.out.println("Draw");
         }        
         currentPlayer = (currentPlayer == oo) ? xx : oo;
   }   
   public static void input(int a) {
      boolean validInput = false;  
      do {
         if (a == oo) {
            System.out.print("Turn O"+"\n"+"Please input row,column: ");            
         } else {
            System.out.print("Turn X"+"\n"+"Please input row,column: ");           
         }
         int row = in.nextInt() - 1;  
         int col = in.nextInt() - 1;
         if (row >= 0 && row < rows && col >= 0 && col < column 
                 && board[row][col] == EMPTY) {currentRow = row;currentCol = col;
            board[currentRow][currentCol] = a;  
            validInput = true;  
         } else {
            System.out.println("Error,Try again");
         }
      } while (!validInput);  
   }
   public static void updateGame(int a, int crRow, int crCol) {
      if (checkWin(a, crRow, crCol)) {  
         currentState = (a == oo) ? CROSS_WON : NOUGHT_WON;
      } else if (isDraw()) {  
         currentState = DRAW;
      }      
   }   
   public static boolean isDraw() {
      for (int row = 0; row < rows; ++row) {
         for (int col = 0; col < column; ++col) {
            if (board[row][col] == EMPTY) {
               return false;  
            }
         }
      }
      return true;  
   }  
   public static boolean checkWin(int a, int crRow, int crCol) {
      return (board[crRow][0] == a && board[crRow][1] == a 
              && board[crRow][2] == a || board[0][crCol] == a 
              && board[1][crCol] == a && board[2][crCol] == a || crRow == crCol 
              && board[0][0] == a && board[1][1] == a && board[2][2] == a 
              || crRow + crCol == 2 && board[0][2] == a && board[1][1] == a 
              && board[2][0] == a );
   }  
   public static void showBoard() {
      for (int row = 0; row < rows; ++row) {
         for (int col = 0; col < column; ++col) {
            showAns(board[row][col]); 
            if (col != column - 1) {
               System.out.print("|");  
            }
         }
         System.out.println();
         if (row != rows - 1) {
            System.out.println(" - + - + - "); 
         }
      }     
   }   
  public static void showAns(int content) {
      switch (content) {
         case EMPTY:  System.out.print("   "); break;
         case xx: System.out.print(" X "); break;
         case oo:  System.out.print(" O "); break;
      }
  }
}